package tablify

import (
	"bytes"
	"fmt"
	"image"
	"image/color"
	_ "image/png"
)

type Result struct {
	rects [][]Rect
	img   image.Image
}

func htmlColor(c color.Color) string {
	r, g, b, a := c.RGBA()
	var alpha float32 = float32(a) / float32(65535)
	return fmt.Sprintf("rgba(%d,%d,%d,%f)", r>>8, g>>8, b>>8, alpha)
}

func (rs *Result) HTML() string {
	//MAX_SIZE := 640
	//PER_PXL_X := MAX_SIZE / rs.img.Bounds().Max.X
	//PER_PXL_Y := MAX_SIZE / rs.img.Bounds().Max.Y
	var buffer bytes.Buffer
	buffer.WriteString("<html><body style='display:inline-block'>")
	buffer.WriteString(rs.Table())
	buffer.WriteString("</body></html>")
	return buffer.String()

}
func (rs *Result) Table() string {
	var buffer bytes.Buffer
	buffer.WriteString("<table style='border-spacing: 0;'>")
	for _, xrow := range rs.rects {
		buffer.WriteString("<tr>\n")
		for _, rect := range xrow {
			buffer.WriteString(fmt.Sprintf("<td colspan='%d' rowspan='%d' style='background-color:%s'>\n</td>", rect.width, rect.height, htmlColor(rect.color)))
			//buffer.WriteString(fmt.Sprintf("<div style='min-width:%dpx; min-height:%dpx;background-color:%s'>&nbsp;</div>\n", rect.width*PER_PXL_X, rect.height*PER_PXL_Y, htmlColor(rect.color)))
			buffer.WriteString("</td>")
		}
		buffer.WriteString("</tr>")
	}
	buffer.WriteString("</table>")
	return buffer.String()
}

type Rect struct {
	x, y          int
	width, height int
	color         color.Color
}

func (rect Rect) String() string {
	return fmt.Sprintf("(%d,%d)@(%dx%d)C@(%v)", rect.x, rect.y, rect.width, rect.height, rect.color)
}

func boolify(img image.Image) [][]bool {
	min, max := img.Bounds().Min, img.Bounds().Max
	bools := make([][]bool, max.Y-min.Y)
	for idx := range bools {
		bools[idx] = make([]bool, max.X-min.X)
	}
	return bools
}

func Tablify(img image.Image) Result {
	min, max := img.Bounds().Min, img.Bounds().Max
	rects := make([][]Rect, 0, max.Y-min.Y)
	bools := boolify(img)
	for y := min.Y; y < max.Y; y += 1 {
		rowRect := make([]Rect, 0)
		for x := min.X; x < max.X; x += 1 {
			if bools[y][x] {
				//if we've already 'scanned' it, just keep moving
				continue
			}
			rect := rectify(img, bools, x, y)

			rowRect = append(rowRect, rect)
		}
		rects = append(rects, rowRect)

	}
	return Result{rects, img}
}

func rectify(img image.Image, bools [][]bool, x, y int) Rect {
	r := Rect{x, y, 1, 1, img.At(x, y)}
	_, max := img.Bounds().Min, img.Bounds().Max
	bools[y][x] = true
	/* we can expand IF,
	within Bounds
	pixel not used
	pixel same color
	*/
	canExpand := func(plusX, plusY int) bool {
		newX, newY := x+(r.width-1)+plusX,
			y+(r.height-1)+plusY
		return newX < max.X && newY < max.Y && // within bounds
			!bools[newY][newX] && // not used
			r.color == img.At(newX, newY) // same color
	}
	for canExpand(1, 0) {
		// just set the flag now
		bools[y+r.height-1][x+r.width] = true
		r.width += 1 // if we can expand right, just do it
	}

	for canExpand(0, 1) {
		rowGood := true
		for i := 0; i < r.width; i += 1 {
			rowGood = rowGood && canExpand(-i, 1)
		}
		if rowGood {
			for i := 0; i < r.width; i++ {
				bools[y+r.height][x+i] = true
			}
			r.height += 1
		} else {
			break
		}
	}
	return r
}
