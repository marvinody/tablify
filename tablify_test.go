package main

import (
	"fmt"
	"image"
	"image/color"
	"testing"
)

func TestTablifyGrayscaleSimple(t *testing.T) {
	c := func(u uint8) color.Gray {
		return color.Gray{u}
	}
	data := [][]color.Gray{
		[]color.Gray{c(255), c(255), c(255), c(255)},
		[]color.Gray{c(255), c(0), c(0), c(255)},
		[]color.Gray{c(255), c(0), c(0), c(255)},
		[]color.Gray{c(255), c(255), c(255), c(255)},
	}
	grayImg := image.NewGray(image.Rectangle{
		image.Point{0, 0},
		image.Point{4, 4},
	})
	for y, xrow := range data {
		for x, clr := range xrow {
			grayImg.Set(x, y, clr)
		}
	}
	r := func(x, y, width, height int, color color.Color) Rect {
		return Rect{x, y, width, height, color}
	}
	rs := Tablify(grayImg)
	expectedRects := [][]Rect{
		[]Rect{r(0, 0, 4, 1, c(255))},
		[]Rect{r(0, 1, 1, 3, c(255)), r(1, 1, 2, 2, c(0)), r(3, 1, 1, 3, c(255))},
		[]Rect{},
		[]Rect{r(1, 3, 2, 1, c(255))},
	}
	for yrowIdx, xrow := range expectedRects {
		for xrowIdx, expectedRect := range xrow {
			x, y := expectedRect.x, expectedRect.y
			rect := rs.rects[yrowIdx][xrowIdx]
			if expectedRect != rect {
				t.Errorf("Error at (%d,%d) got [%dx%d] expected [%dx%d]", x, y,
					rect.width, rect.height, expectedRect.width, expectedRect.height)
			}
		}
	}

}

func prettyPrint(rects [][]Rect) {
	for _, xrow := range rects {
		for _, rect := range xrow {
			fmt.Printf("%v, ", rect)
		}
		fmt.Printf("\n")
	}
	fmt.Printf("\n")
}
