package main

import (
	"bitbucket.org/marvinody/tablify/tablify"
	"bufio"
	"fmt"
	"image"
	_ "image/png"
	"log"
	"net/http"
	"os"
	"text/template"
	"io"
)

func main() {
	if len(os.Args) > 1 {
		// start server to allow hosting
		fmt.Println("Arguments detected, attempting to start server")
		http.HandleFunc("/", RenderIndex)
		http.HandleFunc("/render", RenderRender)
		log.Fatal(http.ListenAndServe(":8778", nil))

	} else {
		// read stdin and decode image
		reader := bufio.NewReader(os.Stdin)
		img, _, err := image.Decode(reader)
		if err != nil {
			panic(err)
		}
		rs := tablify.Tablify(img)
		fmt.Println(rs.HTML())
	}

}

// render the image with correct data. bad name tho...
func RenderRender(w http.ResponseWriter, req *http.Request) {
	url := req.FormValue("url")
	var reader *bufio.Reader
	if url != "" {
		fmt.Println("fetching get url:", url)
		resp, err := http.Get(url)
		if err != nil {
			RenderError(err, w, req)
			return
		}
		defer resp.Body.Close()
		reader = bufio.NewReader(resp.Body)
	} else {
		file, _, err := req.FormFile("file")
		if err != nil {
			RenderError(err, w, req)
			return
		}
		defer file.Close()
		// not sure but I think making it into a bufio, it'll throw an error early if encountered instead of reading whole file
		reader = bufio.NewReader(file)

	}
	fmt.Println("Translating file to image")
	img, _, err := image.Decode(reader)
	if err != nil {
		RenderError(err, w, req)
		return
	}
	fmt.Println("Tablifying image")
	rs := tablify.Tablify(img)
	// probably change the following to something else to allow templates later
	io.WriteString(w, rs.HTML())
}

// Render default page with choice for post or get
func RenderIndex(w http.ResponseWriter, req *http.Request) {
	tmpl, err := template.ParseFiles("templates/index.html")
	if err != nil {
		RenderError(err, w, req)
		return
	}
	err = tmpl.Execute(w, nil)
	if err != nil {
		RenderError(err, w, req)
		return
	}
}

func RenderError(err error, w http.ResponseWriter, req *http.Request) {
	// well I hope we don't have an error here
	fmt.Println("Error detected:", err.Error())
	tmpl, _ := template.ParseFiles("templates/error.html")
	_ = tmpl.Execute(w, struct{
		ErrMsg string
	}{err.Error()})
}
